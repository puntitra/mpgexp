"""
Filename: logging_config.py
Description: Logger configuration
"""

import logging
import logging.config


## TODO: Move to a config file 
LOG_LEVELS = {'debug': logging.DEBUG,
              'info': logging.INFO,
              'warning': logging.WARNING,
              'error': logging.ERROR,
              'critical': logging.CRITICAL
             }

log_level = LOG_LEVELS['warning']
log_file = 'mpgexp.log'
log_maxbytes = 1048576
log_backup = 2
log_format = '[%(asctime)s] %(levelname)s :: %(module)s.%(funcName)s :: %(message)s'
date_format = '%m/%d/%y %H:%M:%S'


LOG_CONFIG = {
    'version': 1,
    'loggers': {
        '': {                   # root logger
            'level': LOG_LEVELS['debug'],
            'handlers': ['stream_handler'],
            'disable_existing_loggers': False,
            'propagate': False
        },
        'future_logger': {      # for future
            'level': log_level,
            'handlers': [],
            'propagate': False
        }
    },
    'handlers': {
        'stream_handler': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
            'level': log_level,
            'formatter': 'log_format'
        },
    },
    'formatters': {
        'log_format': {
            'format': log_format,
            'datefmt': date_format
        },
    }
}


logging.config.dictConfig(LOG_CONFIG)
