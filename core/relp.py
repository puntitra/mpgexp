"""
Filename: relp.py
Description: Relationship analysis methods.
"""

import math
import pandas as pd
import numpy as np
import scipy.stats
from collections import Counter
from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.model_selection import learning_curve, cross_validate


def plot_heatmap(df_corr, figsize=(20, 20), is_half=True):
    """
    Plots heatmap from the given correlation or association matrix.
    Arguments:
    df_corr -- Pandas correlation  matrixfigsize -- (optional) Figure size in tuple format (float, float)
    half -- (optional) plot diagonal matrix if True, otherwise plot full matrix
    """
    assert isinstance(df_corr, pd.DataFrame)

    _, _ = plt.subplots(figsize=figsize)

    # To display diagonal matrix instead of full matrix.
    mask = np.zeros_like(df_corr, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = is_half

    # Generate a custom diverging colormap.
    cmap = sns.diverging_palette(220, 10, as_cmap=True)

    # Draw the heatmap with the mask and correct aspect ratio.
    sns.heatmap(df_corr, mask=mask, cmap=cmap, vmax=1, center=0, annot=True,
                fmt='.2f', square=True, linewidths=.5, cbar_kws={"shrink": .5})


def get_correlated_features(df_corr, threshold=0.95):
    """
    Returns correlated features greater than the threshold.

    Parameters:
    df_corr -- Pandas DataFrame of correlation matrix
    threshold -- correlation threshold [0, 1]

    Returns:
    corr_features -- list of tuples of correlated features

    Note: this method returns a pair of features per one correlation coefficient.
    For example, if 'A' and 'B' are correlated, ('A', 'B') will be added to
    corr_features.
    """
    assert isinstance(df_corr, pd.DataFrame)

    # Select diagonal matrix of correlation matrix.
    lower = df_corr.where(np.tril(np.ones(df_corr.shape), k=-1).astype(np.bool))

    # Find index of columns with correlation equal or over the threshold.
    corr_features = []
    for col in lower:
        for row, coef  in lower[col].iteritems():
            if not np.isnan(coef):
                if abs(coef) > threshold and col != row:
                    corr_features.append((col, row))
    return corr_features


def theils_u_matrix(df_nom):
    """
    Calculates association matrix of Theil's U statistics.

    Parameters:
    df -- Pandas DataFrame (No missing values) with nominal columns

    Returns:
    Pandas DataFrame of association matrix
    """
    assert isinstance(df_norm, pd.DataFrame)

    dim = df_nom.shape[1]
    cols = list(df_nom.columns)

    if df_nom.isna().values.any():
        raise ValueError('Missing values are not allowed in the DataFrame passed in.')

    if dim < 2:
        raise ValueError('At least two nominal columns must be provided.')

    asso = pd.DataFrame(index=cols, columns=cols)
    const_cols = []

    for col in cols:
        if df_nom[col].unique().size == 1:
            const_cols.append(col)

    for i in range(0, dim):
        if cols[i] in const_cols:
            asso.loc[:, cols[i]] = 0.
            asso.loc[cols[i], :] = 0.
            continue
        for j in range(0, dim):
            if cols[i] in const_cols:
                continue
            elif i == j:
                asso.loc[cols[i], cols[j]] = 1.
            else:
                ji = _theils_u(df_nom[cols[i]], df_nom[cols[j]])
                ij = _theils_u(df_nom[cols[j]], df_nom[cols[i]])

                asso.loc[cols[i], cols[j]] = ij if not np.isnan(ij) and abs(ij) < np.inf else 0.
                asso.loc[cols[j], cols[i]] = ji if not np.isnan(ji) and abs(ji) < np.inf else 0.

    asso.fillna(value=np.nan, inplace=True)

    return asso


def _theils_u(x, y):
    """
    Calculates Theil's U statistics for association between nominal variables. In other words,
    it calculates uncertainty coefficient of x given y [0, 1]. 1 means y provides full information
    about x, whereas 0 means y provides no information about x. Note: U(x, y) != U(y, x)
    Source: https://en.wikipedia.org/wiki/Uncertainty_coefficient

    Parameters:
    x -- list / NumPy ndarray / Pandas Series, A sequence of measurements
    y -- list / NumPy ndarray / Pandas Series, A sequence of measurements

    Returns:
    float [0, 1]
    """
    ce_xy = _cond_entropy(x, y)
    x_counter = Counter(x)
    total = sum(x_counter.values())
    p_x = list(map(lambda n: n/total, x_counter.values()))
    s_x = scipy.stats.entropy(p_x)

    if s_x == 0:
        return 1.
    else:
        return (s_x - ce_xy) / s_x


def _cond_entropy(x, y, log_base=math.e):
    """
    Calculates the conditional entropy of x given y: S(x|y)
    Source: https://en.wikipedia.org/wiki/Conditional_entropy

    Parameters:
    x -- list / NumPy ndarray / Pandas Series, A sequence of measurements
    y -- list / NumPy ndarray / Pandas Series, A sequence of measurements
    log_base -- float, default = e

    Returns:
    float
    """
    y_counter = Counter(y)
    xy_counter = Counter(list(zip(x, y)))
    total = sum(y_counter.values())
    entropy = 0.

    for xy in xy_counter.keys():
        p_xy = xy_counter[xy] / total
        p_y = y_counter[xy[1]] / total
        entropy += p_xy * math.log(p_y / p_xy, log_base)

    return entropy
