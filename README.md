[TOC]

# MPG EXP: MPG Modeling

This repository offers a simple data science pipeline for predicting miles per gallon including data exploration, data preprocessing, and modeling tuning. It includes utitlity source code, Jupyter notebooks, and test scripts.

Checkout [mpgapi](https://puntitra@bitbucket.org/puntitra/mpgapi.git) for an end-to-end Flask API counterpart of this repository.

## Libraries

The main libraries used for this repository are Numpy, Pandas, scikit-learn, matplotlib, and Jupyter.

## Features

* Data preprocessing using scikit-learn's Pipeline and ColumnTransformer
* Pipeline and model serialization using pickle
* Experiment visualization using matplotlib and seaborn
* Automated test scripts for utility functions

## Setup

Clone this repository.

```bash
git clone https://puntitra@bitbucket.org/puntitra/mpgexp.git

```

Change working directory to mpgexp.
```bash
cd mpgexp
```

Install packages. *Note:* Python3 is required. Also, using virtual environment is highly recommended.
```bash
python3 -m pip install requirements.txt
```

## Dataset

Create data directory under mpgexp.

```bash
mkdir data
```

Download dataset to data directory.

```bash
wget https://archive.ics.uci.edu/ml/machine-learning-databases/auto-mpg/auto-mpg.data -P data
```

## Read-only View

A static HTML file for every IPython notebook for this repository is available under notebooks directory.


## Tests

Pytest test fixtures and scripts are under tests/.

Screenshot of pytest:
![Pytest](docs/pytest.png)
