"""
Filename: test_data_util.py
Description: Unit tests for core/data_util.py
"""

import pytest
from string import ascii_lowercase
from core import data_util


test_data_path = 'data/test.data'
num_col = 9
num_row = 5
default_header = list(range(0, num_col))
custom_header = list(ascii_lowercase[:num_col])


@pytest.fixture
def valid_data_path():
    return test_data_path


@pytest.fixture
def invalid_data_path():
    return 'bogus.txt'


def test_load_data_w_valid_path(valid_data_path):
    df = data_util.load_data(valid_data_path)

    r, c = df.shape
    assert r == num_row
    assert c == num_col


def test_load_data_w_invalid_path(invalid_data_path):
    with pytest.raises(FileNotFoundError):
        df = data_util.load_data(invalid_data_path)


@pytest.mark.parametrize('col_header, expected', [
                          (list('xyz'), default_header),
                          (88, default_header),
                          (None, default_header),
                          (custom_header, custom_header),
                          (default_header, default_header)
                        ])
def test_load_data_w_col_header(valid_data_path, col_header, expected):
    df = data_util.load_data(valid_data_path,
                        column_header=col_header)

    assert list(df.columns) == expected


@pytest.mark.parametrize('col_header, drop_list, expected', [
                          (custom_header, custom_header, 0),
                          (custom_header, list('bgi'), 6 ),
                          (custom_header, list('axy'), 9),
                          (custom_header, None, 9),
                          (custom_header, 11, 9),
                          (None, [0, 1, 8], 9)
                        ])
def test_load_data_w_drop_list(valid_data_path, col_header,
                               drop_list, expected):
    df = data_util.load_data(valid_data_path,
                        column_header=col_header,
                        drop_list=drop_list)

    assert df.shape[1] == expected
