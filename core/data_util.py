"""
Filename: data_util.py
Description: data related utilities.
"""

import logging
import pandas as pd
import numpy as np
from collections.abc import Sequence
from sklearn.impute import SimpleImputer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split


def load_clean_split(data_path, column_header, target, drop_list=None, test_size=0.25, random_state=42):
    """
    Loads, cleans, and splits dataset.

    Parameters:
    data_path -- dataset file path
    column_header -- list of column header
    target -- target variable
    drop_list -- list of features to drop
    test_size -- proportion of test dataset
    random_state -- random seed used to split data

    Returns:
    X_train, X_test, y_train, y_test
    """
    df = load_data(data_path, column_header=column_header, drop_list=drop_list)

    df = df.replace('?', np.nan)

    X = df.drop([target], axis=1)
    y = df[[target]]

    return train_test_split(X, y, test_size=test_size, random_state=random_state)


def load_data(full_path, column_header=None, drop_list=None):
    """
        Load dataset, drop column(s) and split dataset

        Parameters:
        full_path -- dataset file path
        column_header -- list of column header
        drop_list -- list of attributes to drop

        Return:
        df -- Pandas dataframe of loaded dataset
    """
    if not isinstance(column_header, list):
        if column_header is not None:
            logging.warning("Argument 'column_header' must be a list or None. Column header will not be assigned.")
        column_header = []

    if not isinstance(drop_list, list):
        if drop_list is not None:
            logging.warning("Argument 'drop_list' must be a list or None. Column(s) will not be dropped.")
        drop_list = []

    df = pd.read_fwf(full_path, header=None, na_rep='?')

    if df.shape[1] == len(column_header):
        df.columns = column_header
        # Only attempt to drop column(s) if header is successfully assigned.
        drop_str = ', '.join(map(str, drop_list))
        if set(drop_list).issubset(column_header):
            df = df.drop(columns=drop_list)
            logging.info('%s have been dropped.', drop_str)
        else:
            logging.warning('%s will not be dropped. Reason: one or more column does not exist.', drop_str)
    elif len(column_header) != 0:
        logging.warning('Column header will not be assigned to the dataframe. Reason: the number of columns does not match column header.')

    return df


def cast_df(df, to_type):
    """
    Casts feature(s) to specific type.

    Parameters:
    df -- Pandas DataFrame of shape (m, 1)
    to_type -- Datatype to cast feature(s) to (options: 'int', 'float', 'str')

    Returns:
    df -- Casted DataFrame
    """
    assert isinstance(df, pd.DataFrame)

    if to_type == 'int' and df.dtypes.any() != 'int':
        df = df.astype(int)
    elif to_type == 'float' and df.dtypes.any() != 'float':
        df = df.astype(float)
    elif to_type == 'str' and df.dtypes.any() != 'O':
        df = df.astype(str)

    return df


def get_column_names_from_ColumnTransformer(column_transformer):
    """
    Returns column list from ColumnTransformer.

    Modified from: https://github.com/scikit-learn/scikit-learn/issues/12525#issuecomment-640900712
    """
    col_list = []

    # The last transformer is ColumnTransformer's 'remainder'
    for each_transformer in column_transformer.transformers_[:-1]:

        raw_col_name = list(each_transformer[2])

        if isinstance(each_transformer[1], Pipeline):
            # If pipeline, get the last transformer
            transformer = each_transformer[1].steps[-1][1]
        else:
            transformer = each_transformer[1]

        try:
            if isinstance(transformer, OneHotEncoder):
                names = list(transformer.get_feature_names(raw_col_name))
            elif isinstance(transformer, SimpleImputer) and transformer.add_indicator:
                missing_indicator_indices = transformer.indicator_.features_
                missing_indicators = [raw_col_name[idx] + \
                                  '_missing_flag' for idx in missing_indicator_indices]
                names = raw_col_name + missing_indicators
            else:
                names = list(transformer.get_feature_names())

        except AttributeError as error:
            names = raw_col_name

        col_list.extend(names)

    return col_list


def flatten(seq):
    """
    Flattens nested sequences. It does not flatten str, dict, or bytes.
    Examples: flatten([[1, 2, 3], 4, [[5, 6], 7]]) returns [1, 2, 3, 4, 5, 6, 7]
    flatten([[2, 3], {4: 'hoho'}, [[6], 7], 8]) returns [2, 3, {4: 'hoho'}, 6, 7, 8]
    flatten(['hello', 4, [[6], 7], 8]) returns ['hello', 4, 6, 7, 8]

    Parameters:
    seq -- Sequence to be flattened (i.e. list, tuple)

    Returns:
    Generator object of flattened Sequence.
    """
    for ele in seq:
        if isinstance(ele, Sequence) and not isinstance(ele, (str, bytes)):
            yield from flatten(ele)
        else:
            yield ele
