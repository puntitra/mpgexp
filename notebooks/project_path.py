# File: project_path.py
"""
To be able to import a local module located in another directory, include this file in 
the same directory as notebook file(s) and import it in each notebook before importing
any local module.

TL;DR 

Problem: Cannot import a local module in another directory. 
Assumption: ipython3 & python3 have the same version, path, and executable.

Project structure:
/path/to/project
├── config.py
├── core
│   ├── module_to_import.py
│   └── ...
├── notebooks
│   ├── project_path.py*
│   ├── notebook_file.ipynb
│   └── ...

Explanation: The current working directory is set to the directory the notebook is in. This is a 
known behavior in notebook environment. There are many possible workarounds but I like the workaround 
below because it is idempotent and there is no need to add code snippet in the notebook.

Note: __init__.py is no longer needed in Python3. 
"""
import os


# Relative path of project directory to the directory where your notebook files are. 
# This is the only place that you might need to change based on your project structure. 
PROJECT_RELPATH = '..' 
# Absolute path of the directory this file is in.
NOTEBOOK_DIR = os.path.dirname(os.path.abspath(__file__))

os.chdir(NOTEBOOK_DIR)    
os.chdir(PROJECT_RELPATH)