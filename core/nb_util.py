"""
Filename: nb_util.py
Description: Utility functions for notebooks.
"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.model_selection import learning_curve, cross_validate


def plot_learning_curve(estimator, X, y, title=None, cv=5, n_jobs=1,
                        figsize=(20,10), train_sizes=np.linspace(0.1, 1.0, 5)):
    """
    Generate a simple plot of the test and traning learning curve.

    Parameters:
    estimator -- Object type that implements the "fit" and "predict" methods
    X -- array-like, shape (n_samples, n_features) Training vector
    y -- array-like, shape (n_samples,) Target relative to X
    title -- str, Title for the chart (optional, default None)
    cv -- (optional, default 5) int, cross-validation generator or an iterable,
          If an integer is passed, it is the number of folds (defaults to 5)
    n_jobs -- int, Number of jobs to run in parallel (optional, default 1)
    figsize -- graph size (width, height) (optional, default (20, 10))
    train_sizes -- array-like, Relative or absolute numbers of  training examples that will be used
                   to generate the learning curve (optional, default=np.linspace(0.1, 1.0, 5))
    """
    tn_sizes, tn_scores, val_scores = learning_curve(estimator, X, y, cv=cv,
                                                     n_jobs=n_jobs,
                                                     train_sizes=train_sizes)

    # Mean and SD of training set scores
    tn_mean = np.mean(tn_scores, axis=1)
    tn_sd = np.std(tn_scores, axis=1)

    # Mean and SD of validation set scores
    val_mean = np.mean(val_scores, axis=1)
    val_sd = np.std(val_scores, axis=1)

    # Create plot
    plt.style.use('seaborn-whitegrid')
    plt.figure(figsize=figsize)
    plt.xlabel('Training size')
    plt.ylabel('Score')
    if title is not None:
        plt.title(title)

    colors={'tn': 'indianred', 'val': 'cadetblue'}

    # Draw lines
    plt.plot(tn_sizes, tn_mean, 'o-', color=colors['tn'], label='Training score')
    plt.plot(tn_sizes, val_mean, 'o-', color=colors['val'], label='Cross-validation score')

    # Draw bands
    plt.fill_between(tn_sizes, tn_mean - tn_sd,
                     tn_mean + tn_sd, alpha=0.05, color=colors['tn'])
    plt.fill_between(tn_sizes, val_mean - val_sd,
                     val_mean + val_sd, alpha=0.05, color=colors['val'])

    plt.legend(loc="best")


def plot_feature_importances(features, importances, figsize=(8,4), **kwargs):
    """
    Plots feature importances in descending order.

    Parameters:
    features -- list of features of size n_features
    importances -- list or np.ndarray of feature importances of size n_features
    figsize -- graph size (width, height) (optional, default (8, 4))
    **kwargs -- keyword arguments for seaborn.barplot (optional)
    """
    assert isinstance(features, list)
    assert isinstance(importances, list) or isinstance(importances, np.ndarray)
    assert len(features) == len(importances)

    feature_importances = list(zip(features, importances))

    feature_importances = sorted(feature_importances,
                                 key=lambda x: x[1],
                                 reverse=True)

    df_feature_importances = pd.DataFrame(feature_importances,
                                          columns=['feature', 'importance'])

    ax = sns.set(rc={'figure.figsize': figsize})
    ax = sns.set_style('whitegrid')
    ax = sns.barplot(x='importance', y='feature',
                     data=df_feature_importances,
                     **kwargs)


def print_results(estimator, feature_list, title=None,
                  mae_tn=np.nan, r2_tn=np.nan,
                  mae_ts=np.nan, r2_ts=np.nan):

    print()
    print('Results: {}'.format(title))
    print('Model: {}'.format(estimator))
    print('Features: {}'.format(', '.join(feature_list)))
    print()
    print('Train MAE score: %.3f' % mae_tn)
    print('Train R2 score: %.3f' % r2_tn)
    print()
    print('Test MAE score: %.3f' % mae_ts)
    print('Test R2 score: %.3f' % r2_ts)
    print()
